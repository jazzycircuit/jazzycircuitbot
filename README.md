# jazzycircuitbot
## Description
A Twitch chatbot which informs users of upcoming Jazzy Circuit events and spreads awareness of the Jazzy Circuit GiveButter campaign, among other things.

## Status
As of 2023-12-28, this module is completely unworking and is in the process of being repaired.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
